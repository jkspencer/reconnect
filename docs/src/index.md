# re:connect

re:connect is a penpal network automation platform, designed and built for the
[Prisoner Correspondence Network][pcn].

This documentation acts as both a reference for administrators of a re:connect
installation, as well as a reference for developers contributing to re:connect.

For developers, the [*Internals*](./internals/index.md) section contains
information about the internal components of re:connect.

The source code for re:connect, and this documentation, can be found on
GitLab.com at [againstprisons/reconnect][git]. The documentation lives in the
`docs/` subdirectory.

## License

Both re:connect itself, and this documentation, are released under the
[MIT License][license].

[pcn]: https://pcn.nz
[git]: https://gitlab.com/againstprisons/reconnect
[license]: https://gitlab.com/againstprisons/reconnect/blob/master/LICENSE
